<!-- displays individual comment -->
@extends('layouts.app')

@section('content')
	<h1>{{ $comment->post_id }}</h1>
	<p>{{ $comment->body }}</p>
	<a class="btn btn-primary" href="{{ URL::previous() }}">
		Back
	</a>
@endsection
