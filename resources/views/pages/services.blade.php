@extends('layouts.app')

@section('content')
        <h1>{{ $title }}</h1>
        <p>This website is created using Laravel</p> 

        <ul>
        	@foreach($services as $service)
        	<li>{{ $service }}</li>
        	@endforeach
        </ul>
@endsection
