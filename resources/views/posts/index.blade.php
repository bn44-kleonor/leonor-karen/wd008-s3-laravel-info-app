@extends('layouts.app')

@section('content')
<h1>Posts</h1>
	<!-- {{-- dd($comments) --}} -->
	@foreach($posts as $post)
	<!-- {{-- $post->id --}}		 -->
		<div class="card">
			<div class="card-body">
				<h5 class="card-title">
					{{ $post->title }}		
				</h5>
				<p class="cart-text">
					<!-- {{ $post }} -->
					{{ $post->body }}
				</p>
				<a href="/posts/{{ $post->id }}" class="btn btn-primary">
					View Post
				</a>
			</div>
			<div class="footer">
					@foreach($comments as $comment)
						@if($comment->post_id == $post->id)
							<div class="card">
								<div class="card-body">
									<div class="card-text">
										<!-- {{ $comment }} -->
										{{ $comment->body }}
									</div>
									<a href="/comments/{{ $comment->id }}" class="btn btn-primary">
										View Comment
									</a>
								</div>
							</div>
						@endif
					@endforeach
			</div>
		</div>
	@endforeach
	{{ $posts->links() }}
@endsection
