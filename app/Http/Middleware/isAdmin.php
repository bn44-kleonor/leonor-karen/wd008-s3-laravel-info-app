<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Auth::check());
        // dd(Auth::user());
        // dd(Auth::check() && Auth::user()->role == 'admin');
        if(Auth::check() && Auth::user()->role != 'admin'){
            return redirect()->back();
        } elseif(Auth::user() == null) {
            return redirect()->back();
        }
        return $next($request);
    }
}
