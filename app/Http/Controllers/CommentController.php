<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
    public function show($id)
    {
        $comment = Comment::find($id);
        return view('comments.show')->with('comment', $comment);
    }
}
