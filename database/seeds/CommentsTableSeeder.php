<?php

use Illuminate\Database\Seeder;
use App\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Comment::insert([
        	['post_id' => 1, 'body' => 'This is the comment 1 body'],
        	['post_id' => 2, 'body' => 'This is the comment 2 body'],
        	['post_id' => 3, 'body' => 'This is the comment 3 body'],
            ['post_id' => 4, 'body' => 'This is the comment 3 body'],
            ['post_id' => 1, 'body' => 'This is the comment 3 body'],
            ['post_id' => 2, 'body' => 'This is the comment 3 body'],
            ['post_id' => 3, 'body' => 'This is the comment 3 body'],
            ['post_id' => 4, 'body' => 'This is the comment 3 body'],
            ['post_id' => 1, 'body' => 'This is the comment 3 body'],
            ['post_id' => 2, 'body' => 'This is the comment 3 body'],
            ['post_id' => 3, 'body' => 'This is the comment 3 body'],
            ['post_id' => 4, 'body' => 'This is the comment 3 body']
        ]);
    }
}
